//
//  Utils.m
//  AutoScan
//
//  Created by Suman Cherukuri on 1/10/16.
//  Copyright © 2016 ByteSized. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Utils.h"

@interface Utils ()

@end

@implementation Utils

+ (BOOL)isToday:(NSDate*)date {
    NSCalendar* calendar = [NSCalendar currentCalendar];
    unsigned units = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay;
    
    NSDateComponents* dateComp = [calendar components:units fromDate:date];
    NSDateComponents* todayComp = [calendar components:units fromDate:[NSDate date]];
    
    return ([dateComp day] == [todayComp day] && [dateComp month] == [todayComp month] && [dateComp year]  == [todayComp year]);
}

+ (NSString *)allTrim:(NSString *)string {
    return [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

@end