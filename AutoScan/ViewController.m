//
//  ViewController.m
//  AutoScan
//
//  Created by Suman Cherukuri on 9/9/15.
//  Copyright (c) 2015 ByteSized. All rights reserved.
//

#import "ViewController.h"
#import "Utils.h"

@interface ViewController ()
@property (strong, nonatomic) WKWebView *webView;
@property (strong, nonatomic) UIView *footerView;
@property (strong, atomic) NSDictionary *jsMessage;
@property (nonatomic) BOOL longTouch;
@property (nonatomic) BOOL doubleTouch;

//@property (strong, nonatomic) NSTimer* scanRectTimer;

//@property (nonatomic, strong) MTBBarcodeScanner *scanner;
@property (nonatomic, strong) UIView *cameraView;
@property (nonatomic, strong) UIView *cameraControlsView;
@property (nonatomic, strong) UIView *viewOfInterest;
@property (nonatomic, strong) AVAudioPlayer *beep;
@property (nonatomic, strong) UIButton *cameraButton;
@property (nonatomic, strong) UIButton *barcodeButton;
@property (nonatomic, strong) UIButton *videoButton;
@property (nonatomic, strong) UISlider *slider;
@property (nonatomic, strong) UIButton *torchButton;

-(void)updateMTBScanRect;

@end

#define FOOTER_HEIGHT   44

@implementation ViewController

#pragma mark utils
- (CGFloat)statusBarHeight {
    CGSize statusBarSize = [[UIApplication sharedApplication] statusBarFrame].size;
    return MIN(statusBarSize.width, statusBarSize.height);
}

#pragma mark audio
- (void) initAudio {
    if(_beep)
        return;
    NSError *error = nil;
    _beep = [[AVAudioPlayer alloc]
             initWithContentsOfURL:
             [[NSBundle mainBundle]
              URLForResource: @"scan"
              withExtension: @"wav"]
             error: &error];
    if (error != nil) {
        NSLog(@"scan.wav is not found");
    }
    if(!_beep) {
        NSLog(@"loading sound");
    }
    else {
        _beep.volume = .5f;
        [_beep prepareToPlay];
    }
}

- (void) playBeep {
    if(!_beep)
        [self initAudio];
    [_beep play];
}

#pragma mark - Scanner

- (MTBBarcodeScanner *)sharedScanner {
    static MTBBarcodeScanner *_sharedScanner = nil;
    
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedScanner = [[MTBBarcodeScanner alloc] initWithPreviewView:_cameraView];
        // Optionally set a rectangle of interest to scan codes. Only codes within this rect will be scanned.
        _sharedScanner.scanRect = self.viewOfInterest.frame;
    });
    return _sharedScanner;
}

- (void)startScan {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    //show camera on double tap
    //        static BOOL doubleTouch = NO;
    if (_doubleTouch == NO) {
        _doubleTouch = YES;
        _longTouch = YES;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [NSThread sleepForTimeInterval:0.3f];
            dispatch_async(dispatch_get_main_queue(), ^{
                _doubleTouch = NO;
                BOOL longTouchtoScan = [defaults boolForKey:@"LongTouch"];
                if (_longTouch == YES && longTouchtoScan == YES) {
                    [self startMTBScan];
                }
                return;
            });
        });
        return;
    }
    
    BOOL doubleTapToScan = [defaults boolForKey:@"DoubleTap"];
    if (doubleTapToScan == YES) {
        [self startMTBScan];
    }
    
    //  Too slow to adjust scan rectangle on the fly
    //        if (self.scanRectTimer == nil) {
    //            self.scanRectTimer = [NSTimer scheduledTimerWithTimeInterval:2.0
    //                 target:self selector:@selector(updateMTBScanRect) userInfo:nil repeats:YES];
    //        }
    
}

- (void)startMTBScan {
#ifdef AUTOSCAN_LITE_MODE
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    //allow 10 scans per day.  After that prompt a dialog promoting real version
    NSMutableDictionary *usage = [[defaults dictionaryForKey:@"AUTOSCAN_USAGE"] mutableCopy];
    if (usage == nil || [Utils isToday:[usage objectForKey:@"TODAY"]] == NO) {
        usage = [[NSMutableDictionary alloc] init];
        [usage setObject:[NSDate date] forKey:@"TODAY"];
        [usage setObject:[NSNumber numberWithInt:0] forKey:@"COUNT"];
    }
    else {
        NSNumber *count = (NSNumber *)[usage objectForKey:@"COUNT"];
        if (count == nil) {
            count = [NSNumber numberWithInt:0];
        }
        int count_ = [count intValue];
        if (count_++ >= 10) {
            NSLog(@"Daily limit reached");
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"ByteSized AutoScan" message:@"Thank you for using AutoScan.  Your daily limit has reached.  If you want unlimited scans every day, please upgrade to paid version" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            alertView.alertViewStyle = UIAlertViewStyleDefault;
            [alertView setTag:1];
            [alertView show];
            return;
        }
        [usage setObject:[NSNumber numberWithInt:count_] forKey:@"COUNT"];
        [defaults setObject:usage forKey:@"AUTOSCAN_USAGE"];
        [defaults synchronize];
    }
    
#endif
    [self.view endEditing:YES];
    _cameraView.hidden = NO;
    _cameraControlsView.hidden = NO;
    
    @try {
        [self sharedScanner].didStartScanningBlock = ^{
            //            NSLog(@"The scanner started scanning!");
            _footerView.hidden = YES;
        };
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        BOOL autoScan = [defaults boolForKey:@"AutoScan"];
        if (autoScan == NO) {
            _barcodeButton.hidden = NO;
            [[self sharedScanner] startScanningWithResultBlock:nil];
            return;
        }
        _barcodeButton.hidden = YES;
        [[self sharedScanner] startScanningWithResultBlock:^(NSArray *codes) {
            if ([codes count] >= 1) {
                [self stopScan];
                AVMetadataMachineReadableCodeObject *code = [codes objectAtIndex:0];
                if (code != nil) {
                    NSLog(@"Code = %@:%@", code.type, code.stringValue);
                    [self playBeep];
                    [self sendJSMessage:code.stringValue];
                }
            }
        }];
    }
    @catch(NSException *e) {
        NSLog(@"%@", e);
    }
}

- (void)stopScan {
    [self stopMTBScanning];
}

- (void)stopMTBScanning {
    _cameraView.hidden = YES;
    _cameraControlsView.hidden = YES;
    _slider.value = 1.0;
    //    [self.scanRectTimer invalidate];
    //    self.scanRectTimer = nil;
    [[self sharedScanner] setTorchMode:MTBTorchModeOff];
    [_torchButton setSelected:NO];
    [[self sharedScanner] stopScanning];
    //    _cameraButton.hidden = NO;
    //    _videoButton.hidden = YES;
}

- (void)updateMTBScanRect {
    _viewOfInterest = [[UIView alloc] initWithFrame:CGRectMake(10, (_cameraView.frame.size.height*3/8) - 50, _cameraView.frame.size.width - 20, _cameraView.frame.size.height/4)];
    [_viewOfInterest setBackgroundColor:[UIColor colorWithRed:0.0 green:0.5 blue:0.0 alpha:0.3]];
    _viewOfInterest.layer.borderColor = [UIColor redColor].CGColor;
    _viewOfInterest.layer.borderWidth = 1.0f;
    
    CGFloat maxWidth = _cameraView.frame.size.width - 20;
    CGFloat maxHeight = _cameraView.frame.size.height / 4;
    
    CGRect scanRect = self.sharedScanner.scanRect;
    CGFloat currentScale = scanRect.size.width / maxWidth;
    CGFloat newScale = currentScale * 5.0 / 7.0;
    if (newScale < 0.2) {
        newScale = 1.0;
    }
    
    CGFloat newWidth = maxWidth * newScale;
    CGFloat newHeight = maxHeight * newScale;
    CGFloat xOrigin = (_cameraView.frame.size.width - newWidth) / 2;
    CGFloat yOrigin = (_cameraView.frame.size.height - newHeight) / 2;
    
    CGRect newScanRect = CGRectMake(xOrigin, yOrigin, newWidth, newHeight);
    self.sharedScanner.scanRect = newScanRect;
    
    NSLog(@"Set new scan rect %f,%f %fx%f ", xOrigin, yOrigin, newWidth, newHeight);
}

- (void)barcodeData:(NSString *)barcode isotype:(NSString *)isotype {
    [self sendJSMessage:[NSString stringWithFormat:@"%@", barcode]];
    [self sendJSMessage:barcode];
}

//-(void)barcodeNSData:(NSData *)barcode type:(int)type {
//    mainTabBarController.selectedViewController=self;
//
//	[status setString:@""];
//
//	[status appendFormat:@"Type: %d\n",type];
//	[status appendFormat:@"Type text: %@\n",[dtdev barcodeType2Text:type]];
//	[status appendFormat:@"Barcode: %@",[self toHexString:(uint8_t *)barcode.bytes length:barcode.length space:true]];
//	[displayText setText:status];
//
//	[self updateBattery];
//}

- (void)barcodeData:(NSString *)barcode type:(int)type {
    [self sendJSMessage:[NSString stringWithFormat:@"%@", barcode]];
}

#pragma mark - Overlay Views
- (void)addCameraViewButtons {
    //    _cameraControlsView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 60, self.view.frame.size.width, 60)];
    
    _cameraControlsView = [[UIView alloc] initWithFrame:CGRectMake(_viewOfInterest.frame.origin.x + 1, _viewOfInterest.frame.origin.y + _viewOfInterest.frame.size.height - 42, _viewOfInterest.frame.size.width - 2, 41)];
    
    //    [_cameraControlsView setBackgroundColor:[UIColor colorWithWhite:0.6 alpha:1]];
    [_cameraControlsView setBackgroundColor:[UIColor colorWithRed:0.0 green:0.5 blue:0.0 alpha:0.6]];
    
    [_cameraView addSubview:_cameraControlsView];
    _cameraControlsView.hidden = NO; //YES;
    
    UIImage *cancelImage = [UIImage imageNamed:@"Cancel-32.png"];
    UIImage *torchImage = [UIImage imageNamed:@"Torch-32.png"];
    UIImage *torchImageFilled = [UIImage imageNamed:@"Torch-Filled-32.png"];
    UIImage *barcodeImage = [UIImage imageNamed:@"Barcode-Scanner-32.png"];
    
    _torchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _torchButton.frame = CGRectMake(64, 5, 32, 32);
    [_torchButton setImage:torchImage forState:UIControlStateNormal];
    [_torchButton setImage:torchImageFilled forState:UIControlStateSelected];
    [_torchButton addTarget:self action:@selector(onTorch:) forControlEvents:UIControlEventTouchDown];
    [_cameraControlsView addSubview:_torchButton];
    
    _barcodeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _barcodeButton.frame = CGRectMake(_cameraControlsView.frame.size.width/2 - 16, 5, 32, 32);
    [_barcodeButton setImage:barcodeImage forState:UIControlStateNormal];
    [_barcodeButton addTarget:self action:@selector(onBarcodeScanner:) forControlEvents:UIControlEventTouchDown];
    [_cameraControlsView addSubview:_barcodeButton];
    
    //    _cameraButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    _cameraButton.frame = CGRectMake(_cameraControlsView.frame.size.width/2 - 25, 5, 50, 50);
    //    [_cameraButton setImage:cameraImage forState:UIControlStateNormal];
    //    [_cameraButton addTarget:self action:@selector(onFreeze:) forControlEvents:UIControlEventTouchDown];
    //    [_cameraControlsView addSubview:_cameraButton];
    //
    //    _videoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    _videoButton.frame = CGRectMake(_cameraView.frame.size.width/2 - 25, 5, 50, 50);
    //    [_videoButton setImage:videoImage forState:UIControlStateNormal];
    //    [_videoButton addTarget:self action:@selector(onUnFreeze:) forControlEvents:UIControlEventTouchDown];
    //    [_cameraControlsView addSubview:_videoButton];
    //    _videoButton.hidden = YES;
    
    UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelButton.frame = CGRectMake(_cameraControlsView.frame.size.width - 96, 5, 32, 32);
    [cancelButton setImage:cancelImage forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(onCancel:) forControlEvents:UIControlEventTouchDown];
    [_cameraControlsView addSubview:cancelButton];
}

- (void)sliderAction:(UISlider*)slider {
    [[self sharedScanner] zoom:(CGFloat)slider.value];
}

- (void)onTorch:(UIButton *)sender {
    MTBTorchMode curMode = [[self sharedScanner] torchMode];
    if (curMode == MTBTorchModeOff) {
        [[self sharedScanner] setTorchMode:MTBTorchModeOn];
        [_torchButton setSelected:YES];
    }
    else if (curMode == MTBTorchModeOn) {
        [[self sharedScanner] setTorchMode:MTBTorchModeOff];
        [_torchButton setSelected:NO];
    }
}

- (void)onCancel:(UIButton *)sender {
    [self stopMTBScanning];
}

- (void)onFreeze:(UIButton *)sender {
    [[self sharedScanner] freezeCapture];
    sender.hidden = YES;
    _videoButton.hidden = NO;
}

- (void)onUnFreeze:(UIButton *)sender {
    [[self sharedScanner] unfreezeCapture];
    sender.hidden = YES;
    _cameraButton.hidden = NO;
    [[self sharedScanner] zoom:(CGFloat)_slider.value];
}

- (void)onBarcodeScanner:(UIButton *)sender {
    [[self sharedScanner] setResultBlock:^(NSArray *codes) {
        if ([codes count] >= 1) {
            [self stopScan];
            AVMetadataMachineReadableCodeObject *code = [codes objectAtIndex:0];
            if (code != nil) {
                NSLog(@"Code = %@:%@", code.type, code.stringValue);
                [self playBeep];
                [self sendJSMessage:code.stringValue];
            }
        }
    }];
}

#pragma mark viewcontroller

- (void)viewWillDisappear:(BOOL)animated {
    [[self sharedScanner] stopScanning];
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    BOOL oldScanView = YES;
    
    // MTBScanner views
    if (oldScanView) {
        _cameraView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,
                                                               self.view.frame.size.width, self.view.frame.size.height)];
    }
    else {
        _cameraView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,
                                                               self.view.frame.size.width, self.view.frame.size.width / 3)];
    }
    
    [_cameraView setBackgroundColor:[UIColor colorWithWhite: 0.95 alpha:1]];
    
    CGFloat scanWidth = _cameraView.frame.size.width / 2.0;
    CGFloat scanHeight = scanWidth / 3.0;
    CGFloat scanX = (_cameraView.frame.size.width - scanWidth) / 2.0;
    CGFloat scanY = (_cameraView.frame.size.height - scanHeight) / 2.0;
    
    if (oldScanView) {
        _viewOfInterest = [[UIView alloc] initWithFrame:CGRectMake(10, (_cameraView.frame.size.height*3/8) - 130, _cameraView.frame.size.width - 20, _cameraView.frame.size.height/4)];
    }
    else {
        _viewOfInterest = [[UIView alloc] initWithFrame:CGRectMake(scanX, scanY, scanWidth, scanHeight)];
    }
    [_viewOfInterest setBackgroundColor:[UIColor colorWithRed:0.0 green:0.5 blue:0.0 alpha:0.3]];
    _viewOfInterest.layer.borderColor = [UIColor redColor].CGColor;
    _viewOfInterest.layer.borderWidth = 1.0f;
    
    //tap gesture
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(respondToTapGesture:)];
    tap.numberOfTapsRequired = 1;
    tap.delegate = self;
    [_viewOfInterest addGestureRecognizer:tap];
    
    [_cameraView addSubview:_viewOfInterest];
    _cameraView.autoresizesSubviews = NO;
    
    // add slider
//    CGRect frame = CGRectMake(10.0, _cameraView.frame.size.height - 40, _cameraView.frame.size.width - 20, 20.0);
    CGRect frame = CGRectMake(_viewOfInterest.frame.origin.x, _viewOfInterest.frame.origin.y + _viewOfInterest.frame.size.height + 10, _viewOfInterest.frame.size.width, 20.0);
    
    _slider = [[UISlider alloc] initWithFrame:frame];
    [_slider addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
    [_slider setBackgroundColor:[UIColor clearColor]];
    [_slider setMinimumTrackTintColor:[UIColor greenColor]];
    [_slider setMaximumTrackTintColor:[UIColor greenColor]];
    [_slider setThumbTintColor:[UIColor greenColor]];
    _slider.minimumValue = 1.0;
    _slider.maximumValue = [[self sharedScanner] maxScaleFactor]; //4.0;
    _slider.continuous = YES;
    _slider.value = 1.0;
    
    if (oldScanView) {
        [_cameraView addSubview:_slider];
    }
    [self addCameraViewButtons];
    [self.view addSubview:_cameraView];
    _cameraView.hidden = YES;
    _cameraControlsView.hidden = YES;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *appURL = [defaults objectForKey:@"appURL"];
    if (appURL == nil || [appURL isEqualToString:@""]) {
        [self displayURLDialog];
    }
    else {
        [self loadWebViewWithUrl:appURL];
    }
}

- (void) loadContent:(NSString *)appUrl {
    if (appUrl != nil && [[Utils allTrim:appUrl] length] > 0) {
        NSURL *url = [[NSURL alloc] initWithString:appUrl];
        if (![[url scheme] length]) {
            url = [NSURL URLWithString:[@"http://" stringByAppendingString:appUrl]];
        }
        [_webView loadRequest:[NSURLRequest requestWithURL:url]];
    }
    else {
        NSString* path = [[NSBundle mainBundle] pathForResource:@"index"
                                                         ofType:@"html"];
        NSString* content = [NSString stringWithContentsOfFile:path
                                                      encoding:NSUTF8StringEncoding
                                                         error:NULL];
        [_webView loadHTMLString:content baseURL:nil];
    }
}

- (void) displayURLDialog {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *appURL = [defaults objectForKey:@"appURL"];
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"ByteSized AutoScan" message:@"Enter your application URL and press OK.  Use Demo button to load a sample page" delegate:self cancelButtonTitle:@"Demo" otherButtonTitles:@"OK", nil];
    alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    UITextField* tf = [alertView textFieldAtIndex:0];
    tf.keyboardType = UIKeyboardTypeURL;
    tf.text = appURL;
    
    [alertView setTag:2];
    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if ([alertView tag] == 1) {
        return;
    }
    NSString *appURL = nil;
    if (buttonIndex == 1) {
        appURL = [alertView textFieldAtIndex:0].text;
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:appURL forKey:@"appURL"];
        [defaults synchronize];
        if (_webView != nil) {
            [self loadContent:appURL];
        }
        else {
            [self loadWebViewWithUrl:appURL];
        }
    }
    else {
        // demo mode
        if (_webView != nil) {
            [self loadContent:nil];
        }
        else {
            [self loadWebViewWithUrl:nil];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark webview
- (void)loadWebViewWithUrl:(NSString *)appUrl {
    WKWebViewConfiguration *configuration = [[WKWebViewConfiguration alloc] init];
    WKUserContentController *controller = [[WKUserContentController alloc] init];
    
    [controller addScriptMessageHandler:self name:@"bytesized"];
    //inject ByteSized scripts
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"ByteSizedScanner" ofType:@"js"];
    if (filePath) {
        NSError *error;
        NSString *scriptSource = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:&error];
        if (error || scriptSource.length == 0) {
            NSLog(@"Error reading ByteSizedScanner.js: %@", error);
        }
        else {
            //  NSString *scriptSource = @"function byteSizedCallBack(id, value) { window.frames[0].sap.ui.getCore().byId(\"idView1\").getController().byId(id).setValue(value);}";
            
            WKUserScript *userScript = [[WKUserScript alloc]
                                        initWithSource:scriptSource
                                        injectionTime:WKUserScriptInjectionTimeAtDocumentEnd
                                        forMainFrameOnly:NO];
            
            [controller addUserScript:userScript];
            //NSLog(@"%@ is injected:\n%lu", filePath, (unsigned long)scriptSource.length);
        }
    }
    
    configuration.userContentController = controller;
    
    _webView = [[WKWebView alloc] initWithFrame:self.view.frame configuration:configuration];
    [_webView setUIDelegate:self];
    [_webView setNavigationDelegate:self];
    _webView.scrollView.contentInset = UIEdgeInsetsZero;
    _webView.frame = CGRectMake(0, [self statusBarHeight], self.view.frame.size.width, (self.view.frame.size.height - [self statusBarHeight]));// - FOOTER_HEIGHT));
    _webView.allowsBackForwardNavigationGestures = YES;
    
    [self loadContent:appUrl];
    
    [self.view addSubview:_webView];
    _footerView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - FOOTER_HEIGHT, self.view.frame.size.width, FOOTER_HEIGHT)];
    [_footerView setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:_footerView];
    
    UIImage *backImage = [UIImage imageNamed:@"Back-32.png"];
    UIImage *forwardImage = [UIImage imageNamed:@"Forward-32.png"];
    UIImage *homeImage = [UIImage imageNamed:@"Home-32.png"];
    UIImage *refreshImage = [UIImage imageNamed:@"Refresh-32.png"];
    UIImage *helpImage = [UIImage imageNamed:@"Help-32.png"];
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(10, (FOOTER_HEIGHT - 32)/2, 32, 32);
    [backButton setImage:backImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchDown];
    [_footerView addSubview:backButton];
    
    UIButton *forwardButton = [UIButton buttonWithType:UIButtonTypeCustom];
    forwardButton.frame = CGRectMake(self.view.frame.size.width - 42, (FOOTER_HEIGHT - 32)/2, 32, 32);
    [forwardButton setImage:forwardImage forState:UIControlStateNormal];
    [forwardButton addTarget:self action:@selector(goForward:) forControlEvents:UIControlEventTouchDown];
    [_footerView addSubview:forwardButton];
    
    UIButton *homeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    homeButton.frame = CGRectMake(self.view.frame.size.width/2 - 16, (FOOTER_HEIGHT - 32)/2, 32, 32);
    [homeButton setImage:homeImage forState:UIControlStateNormal];
    [homeButton addTarget:self action:@selector(goHome:) forControlEvents:UIControlEventTouchDown];
    [_footerView addSubview:homeButton];
    
    UIButton *refreshButton = [UIButton buttonWithType:UIButtonTypeCustom];
    refreshButton.frame = CGRectMake(self.view.frame.size.width/4 - 6, (FOOTER_HEIGHT - 32)/2, 32, 32);
    [refreshButton setImage:refreshImage forState:UIControlStateNormal];
    [refreshButton addTarget:self action:@selector(goRefresh:) forControlEvents:UIControlEventTouchDown];
    [_footerView addSubview:refreshButton];
    
    
    UIButton *helpButton = [UIButton buttonWithType:UIButtonTypeCustom];
    helpButton.frame = CGRectMake(self.view.frame.size.width*3/4 - 26, (FOOTER_HEIGHT - 32)/2, 32, 32);
    [helpButton setImage:helpImage forState:UIControlStateNormal];
    [helpButton addTarget:self action:@selector(goHelp:) forControlEvents:UIControlEventTouchDown];
    [_footerView addSubview:helpButton];
    
    _footerView.hidden = YES;
    
    //tap gesture
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(respondToTapGesture:)];
    tap.numberOfTapsRequired = 2;
    tap.delegate = self;
    [_webView addGestureRecognizer:tap];
    
    [self.view sendSubviewToBack:_webView];
}

- (void)respondToTapGesture:(UITapGestureRecognizer *)sender {
    if ([sender view] == _webView) {
        if (_cameraControlsView.hidden == NO) {
            //When camera is on, don't show footer control
            _footerView.hidden = YES;
            return;
        }
        if (sender.state == UIGestureRecognizerStateRecognized) {
            _footerView.hidden = !_footerView.hidden;
        }
    }
    else if ([sender view] == _viewOfInterest) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        BOOL autoScan = [defaults boolForKey:@"AutoScan"];
        if (autoScan == NO) {
            [self onBarcodeScanner:nil];
        }
    }
}

- (void)respondToSingleTapGesture:(UITapGestureRecognizer *)sender {
    if (sender.state == UIGestureRecognizerStateEnded) {
        NSLog(@"tap ended");
    }
    else if (sender.state == UIGestureRecognizerStateBegan) {
        NSLog(@"tap started");
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer    shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (void)goBack:(id)sender {
    _footerView.hidden = !_footerView.hidden;
    if ([_webView canGoBack]) {
        [_webView goBack];
    }
}

- (void)goForward:(id)sender {
    _footerView.hidden = !_footerView.hidden;
    if ([_webView canGoForward]) {
        [_webView goForward];
    }
}

- (void)goHome:(id)sender {
    _footerView.hidden = !_footerView.hidden;
}

- (void)goRefresh:(id)sender {
    _footerView.hidden = !_footerView.hidden;
}

- (void)goHelp:(id)sender {
    _footerView.hidden = !_footerView.hidden;
}

- (void)removeCaches {
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *library = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) lastObject];
    NSString *directory = [library stringByAppendingPathComponent:@"Caches/"];
    NSError *error = nil;
    for (NSString *file in [fm contentsOfDirectoryAtPath:directory error:&error]) {
        BOOL success = [fm removeItemAtPath:[NSString stringWithFormat:@"%@/%@", directory, file] error:&error];
        if (!success || error) {
            NSLog(@"cannot delete %@:", file);
        }
    }
}

- (void)sendJSMessage: (NSString *)message {
    if (_jsMessage == nil) {
        return;
    }
    NSString *callback = [_jsMessage objectForKey:@"callback"];
    
    if (callback == nil) {
        NSLog(@"Value for callback is nil.  returning...");
        return;
    }
    NSString *field = [_jsMessage objectForKey:@"id"];
    if (field == nil) {
        field = @"";
    }
    
    NSString *jsTemplate = @"%@(\"%@\", \"%@\");";
    
    NSString *jsFunction = [NSString stringWithFormat:jsTemplate, callback, field, message];
    
    [_webView evaluateJavaScript:jsFunction completionHandler:nil];
    return;
}

- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message {
    if ([message.name isEqualToString:@"bytesized"]) {
        @try {
//            NSLog(@"Received event %@", message.body);
            
            NSDictionary *data = message.body;
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            
            NSString *fieldId = [data objectForKey:@"id"];
            NSString *action = [data objectForKey:@"action"];
            NSString *callback = [data objectForKey:@"callback"];
            
            if (action == nil || callback == nil) {
                NSLog(@"Value for action and/or callback is nil.  returning...");
                return;
            }
            
            if (fieldId != nil && [fieldId caseInsensitiveCompare:@"sap-user"] == NSOrderedSame) {
                return;
            }
            
            if ([action caseInsensitiveCompare:@"startScan"] == NSOrderedSame) {
                _jsMessage = [NSDictionary dictionaryWithDictionary:data];
                [self startScan];
            }
            else if([action caseInsensitiveCompare:@"stopScan"] == NSOrderedSame) {
                _longTouch = NO;
                NSString *scanningMode = [defaults objectForKey:@"scanningMode"];
                if (scanningMode != nil && [@"Camera" caseInsensitiveCompare:scanningMode] != NSOrderedSame) {
                    [self stopScan];
                }
                BOOL longTouchToScan = [defaults boolForKey:@"LongTouch"];
                if (longTouchToScan) {
                    [self.view endEditing:YES];
                }
            }
        }
        @catch(NSException *e) {
            NSLog(@"%@", e);
        }
    }
}

- (void)webView:(WKWebView *)webView runJavaScriptAlertPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(void))completionHandler {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:message message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleCancel
                                handler:^(UIAlertAction *action) {
                                    completionHandler();
                                }]];
    [self presentViewController:alertController animated:YES completion:^{}];
}

- (void)webView:(WKWebView *)webView didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential *))completionHandler {
    
    NSString *hostName = webView.URL.host;
    
    NSString *authenticationMethod = [[challenge protectionSpace] authenticationMethod];
    if ([authenticationMethod isEqualToString:NSURLAuthenticationMethodDefault]
        || [authenticationMethod isEqualToString:NSURLAuthenticationMethodHTTPBasic]
        || [authenticationMethod isEqualToString:NSURLAuthenticationMethodHTTPDigest]) {
        
        NSString *title = @"Authentication Challenge";
        NSString *message = [NSString stringWithFormat:@"%@ requires user name and password", hostName];
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            textField.placeholder = @"User";
        }];
        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            textField.placeholder = @"Password";
            textField.secureTextEntry = YES;
        }];
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            NSString *userName = ((UITextField *)alertController.textFields[0]).text;
            NSString *password = ((UITextField *)alertController.textFields[1]).text;
            
            NSURLCredential *credential = [[NSURLCredential alloc] initWithUser:userName password:password persistence:NSURLCredentialPersistencePermanent];
            
            completionHandler(NSURLSessionAuthChallengeUseCredential, credential);
            
        }]];
        [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            completionHandler(NSURLSessionAuthChallengeCancelAuthenticationChallenge, nil);
        }]];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentViewController:alertController animated:YES completion:^{}];
        });
        
    } else {
        completionHandler(NSURLSessionAuthChallengePerformDefaultHandling, nil);
    }
}

- (void)webView:(WKWebView * _Nonnull)webView
didStartProvisionalNavigation:(WKNavigation * _Null_unspecified)navigation {
//        NSLog(@"didStartProvisionalNavigation");
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
//    NSLog(@"didFinishNavigation");
}

- (void)webView:(WKWebView *)webView
didFailNavigation:(WKNavigation *)navigation
      withError:(NSError *)error {
    
}

- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error {
    NSLog(@"%@", error);
    NSString *errorString = [NSString stringWithFormat:@"<H3>Error loading the URL: %@<br>%@</H3>", [[error userInfo] objectForKey:@"NSErrorFailingURLKey"], [error localizedDescription]];
    [webView loadHTMLString:errorString baseURL:nil];
    [self displayURLDialog];
}

@end
