//
//  Utils.h
//  AutoScan
//
//  Created by Suman Cherukuri on 1/10/16.
//  Copyright © 2016 ByteSized. All rights reserved.
//

#ifndef Utils_h
#define Utils_h

@interface Utils: NSObject

+ (BOOL)isToday:(NSDate *)date;
+ (NSString *)allTrim:(NSString *)string;

@end

#endif /* Utils_h */
