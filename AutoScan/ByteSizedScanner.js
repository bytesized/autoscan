//
//  ByteSizedScanner.js
//
//  Created by Suman Cherukuri on 9/11/15.
//  Copyright (c) 2015 ByteSized. All rights reserved.
//

function byteSizedScanner(message) {
    if (window.webkit && window.webkit.messageHandlers.bytesized) {
        window.webkit.messageHandlers.bytesized.postMessage(message);
    }
}

function byteSizedCallBack(id, val) {
    var element = document.getElementById(id);
    if (element) {
        element.value = val;
    }
    else {
        element = window.frames[0].document.getElementById(id);
        element.value = val;
    }
    element.focus();
    element.blur();
}

function startScan(evt) {
    if (evt.target.readOnly === false && evt.target.disabled === false) {
        var element = evt.target;
        document.getElementById(element.id).prevColor = document.getElementById(element.id).style.borderColor;
        document.getElementById(element.id).style.borderColor = "#FF0000";
        var dictionary = {
            action: "startScan",
            id: element.id,
            callback: "byteSizedCallBack"
        };
        byteSizedScanner(dictionary);
    }
}

function stopScan(evt) {
    if (evt.target.readOnly === false && evt.target.disabled === false) {
        var element = evt.target;
        document.getElementById(element.id).style.borderColor = document.getElementById(element.id).prevColor;
        var dictionary = {
            action: "stopScan",
            id: element.id,
            callback: "byteSizedCallBack"
        };
        byteSizedScanner(dictionary);
    }
}

var inputs = document.getElementsByTagName('input'); 
var i = 0;
for (i = 0; i < inputs.length; i++) {
    if(inputs[i] && inputs[i].tagName &&
       inputs[i].tagName.toLowerCase() === "input" &&
       inputs[i].type.toLowerCase() === "text") {
        inputs[i].addEventListener("touchstart", startScan, false);
        inputs[i].addEventListener("touchend", stopScan, false);
    }
}

var meta = document.createElement('meta');
meta.name = 'viewport';
meta.content = 'width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=no';
var head = document.getElementsByTagName('head')[0];
head.appendChild(meta);

