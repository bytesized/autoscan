//
//  ViewController.h
//  AutoScan
//
//  Created by Suman Cherukuri on 9/9/15.
//  Copyright (c) 2015 ByteSized. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

#import "MTBBarcodeScanner.h"

@interface ViewController : UIViewController <UIGestureRecognizerDelegate, UIAlertViewDelegate, WKScriptMessageHandler, WKUIDelegate, WKNavigationDelegate> {

}

@end

